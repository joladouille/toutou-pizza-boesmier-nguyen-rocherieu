# Toutou Pizza :dog:

The _cheap_ version of Tutti Pizza.

## Démarrer le système

Le code de ce projet est écrit avec [Django](https://www.djangoproject.com/), il s’agit d’un cadre logiciel pour le Web écrit en Python. Le module qui est utilisé pour rendre les services REST est [`djangorestframework`](https://www.django-rest-framework.org/). Ce court document vous permet de démarrer le système pour commencer.

1. Clonez le dépôt ou récupérez ses fichiers. Initialisez un nouveau dépôt le cas échéant.
2. Installez les dépendances Python à l’aide de `pip`. Utilisez le fichier `requirements.txt`.
3. Créez la base de données : par défaut, cela va créer un fichier SQLite : `db.sqlite3`. Pour ceux qui veulent explorer, suivez le tutoriel Django, il est de qualité, et en français. Vous pouvez naviguer dans la base de données avec les outils que vous connaissez ou l’utilitaire `sqlite3` de votre système. Lancez : `./manage.py migrate` (en fait, migre la base de données → prend en compte les modifications de schémas).
4. Créer un utilisateur administrateur. Django fournit une [`interface d’administration`](https://docs.djangoproject.com/fr/3.1/ref/contrib/admin/) simple à utiliser pour manipuler les modèles. Il faut auparavant créer un utilisateur administrateur, lancez la commande `./manage.py createsuperuser`.
5. Enfin, lancez le serveur de développement (qui diffère de la production, comme vous le verrez par la suite !). Lancez : `./manage.py runserver`.



lien vers la doc provisiore : [Documentation - HedgeDoc](https://demo.hedgedoc.org/AGVZDM4ASVaXb_LcW7StoQ)
