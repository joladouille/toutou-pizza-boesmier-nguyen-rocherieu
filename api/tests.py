import json

from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from datetime import datetime
from restaurant.models import Address, Restaurant, Ingredient, Pizza
from online_orders.models import Client, Order


class APIRestaurantTest(TestCase):

    def setUp(self):
        self.address = Address.objects.create(
            id=1,
            nb=10,
            street="de Gaule",
            zipcode=17000,
            city="la Rochelle"
        )
        self.restaurant = Restaurant.objects.create(
            id=1,
            name="test",
            address=self.address
        )
        self.ingredient1 = Ingredient.objects.create(
            id=1,
            name="Tomate"
        )
        self.ingredient2 = Ingredient.objects.create(
            id=2,
            name="Oignon"
        )
        self.pizza = Pizza.objects.create(
            id=1,
            name="test_pizza",
            price=8,
        )
        self.pizza.ingredients.add(self.ingredient1, self.ingredient2)

    def test_Restaurants_exists(self):
        client = APIClient()
        response = client.get(
            reverse("api:api_restaurants")
        )
        self.assertEqual(200, response.status_code)

    def test_created_restaurants_exists(self):
        client = APIClient()
        response = client.get(
            reverse("api:api_restaurants")
        )
        json_data = json.loads(response.content)
        self.assertEqual(1, len(json_data))

    def test_created_restaurants_exists_and_has_correct_values(self):
        client = APIClient()
        response = client.get(
            reverse("api:api_restaurants")
        )
        json_data = json.loads(response.content)
        retaurant = json_data[0]

        self.assertEqual(self.restaurant.name, retaurant.get("name"))

    def test_Pizza_exists(self):
        client = APIClient()
        response = client.get(
            reverse("api:api_pizzas")
        )
        self.assertEqual(200, response.status_code)

    def test_created_pizzas_exists(self):
        client = APIClient()
        response = client.get(
            reverse("api:api_pizzas")
        )
        json_data = json.loads(response.content)
        self.assertEqual(1, len(json_data))

    def test_created_pizzas_exists_and_has_correct_values(self):
        client = APIClient()
        response = client.get(
            reverse("api:api_pizzas")
        )
        json_data = json.loads(response.content)
        pizza = json_data[0]

        self.assertEqual(self.pizza.name, pizza.get("name"))


class APIOrderTest(TestCase):
    def setUp(self):
        self.address1 = Address.objects.create(
            id=1,
            nb=10,
            street="de Gaule",
            zipcode=17000,
            city="la Rochelle"
        )

        self.address2 = Address.objects.create(
            id=2,
            nb=12,
            street="Rue",
            zipcode=17000,
            city="la Rochelle"
        )

        self.restaurant = Restaurant.objects.create(
            id=1,
            name="test",
            address=self.address1
        )

        self.client = Client.objects.create(
            id=1,
            name="Jacques",
            email="jacques@exemple.com"
        )

        self.ingredient = Ingredient.objects.create(
            id=1,
            name="tomate",
            allergen=False
        )

        self.pizza = Pizza.objects.create(
            id=1,
            name="pizza1",
            price=10
        )

        self.pizza.ingredients.add(self.ingredient)

        self.order = Order.objects.create(
            id=1,
            client=self.client,
            restaurant=self.restaurant,
            address=self.address2,
            delivery_hour=datetime(2015, 6, 15)
        )

        self.order.pizzas.add(self.pizza)

    def test_Order_exists(self):
        client = APIClient()
        response = client.get("/api/v1/orders/1")
        self.assertEqual(200, response.status_code)
