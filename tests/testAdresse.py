from django.test import TestCase
from restaurant.models import Address

class AddressTestCase(TestCase):
	def setUp(self):
		Address.objects.create(nb = 5, street = "Rue", complement = "complement", city = "LR", zipcode = 100)
	def test_Address__str__(self):
		adresse1 = Address.objects.get(nb=5)
		self.assertEqual(adresse1.__str__(), "5, Rue (complement) - LR (100)")  

