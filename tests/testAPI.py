import json

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

#from profile.api.models import profile
#from profile.models import Profile

class test_registrationTestCase(APITestCase):
    
    def test_liste_pizzas(self):
        response = self.client.get("/api/v1/pizzas")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_restaurants(self):
        response = self.client.get("/api/v1/restaurants")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
"""
    def test_ajouter_order(self):
        data = {
            "client": {
                "name": "michel",
                "email": "michel@gmal.com"
            },
            "address": {
                "nb": 1,
                "street": "goélan",
                "complement": "adress complement",
                "city": "La Rochelle",
                "zipcode": 17000
            },
            "delivery_hour": '2021-02-03T14:53',
            "restaurant": 1,
            "pizzas": [1]
        }"""