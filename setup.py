import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="toutoutpizza", # Replace with your own username
    version="0.0.1",
    author="Clement B, Anh Thu N, Theo R",
    author_email="cle.chec@laposte.net",
    description="Learn about devOps",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/joladouille/toutou-pizza-boesmier-nguyen-rocherieu",
    project_urls={
        "Bug Tracker": "https://gitlab.com/joladouille/toutou-pizza-boesmier-nguyen-rocherieu/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"toutou_pizza": "toutou_pizza","api":"api","online_orders":"online_orders","restaurant":"restaurant"},
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
)