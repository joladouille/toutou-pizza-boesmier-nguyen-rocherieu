from django.test import TestCase

from online_orders.models import Client, Order
from restaurant.models import Restaurant, Address, Ingredient, Pizza
from datetime import datetime

class Online_ordersTestCase(TestCase):

    def setUp(self):
        self.address = Address.objects.create(
            id=1,
            nb=10,
            street="rue de Gaule",
            zipcode=17000,
            city="la Rochelle"
        )
        self.address2 = Address.objects.create(
            id=2,
            nb=20,
            street="rue des pré",
            zipcode=17000,
            city="la Rochelles"
        )

        self.client1 = Client.objects.create(
            id=1,
            name="michel",
            email="michel.dupond@gmiel.com"
        )
        self.restaurant = Restaurant.objects.create(
            name="toutoupizz",
            address=self.address
        )

        self.ingredient1 = Ingredient.objects.create(
            id=1,
            name="Tomate"
        )

        self.ingredient2 = Ingredient.objects.create(
            id=2,
            name="Oignon"
        )

        self.pizza = Pizza.objects.create(
            id=1,
            name="test_pizza",
            price=8,
        )

        self.pizza.ingredients.add(self.ingredient1, self.ingredient2)

        self.order = Order.objects.create(
            client=self.client1,
            restaurant=self.restaurant,
            address=self.address2,
            delivery_hour= datetime(2015, 6, 15)
        )
        self.order.pizzas.add(self.pizza)


    def test_client_existe(self):
        clients = Client.objects.get(pk=1)
        self.assertEqual("michel (michel.dupond@gmiel.com)",str(clients))

    def test_order_existe(self):
        orders = Order.objects.get(pk=1)
        self.assertEqual("Order for michel at 2015-06-14 22:00:00+00:00, 20, rue des pré () - la Rochelles (17000)",str(orders))