from django.test import TestCase
from restaurant.models import Restaurant, Address, Ingredient, Pizza


class RestaurantTestCase(TestCase):

    def setUp(self):
        adresse = Address.objects.create(
            id=1,
            nb=16,
            street="rue des peuplier",
            zipcode="85000",
            city="la roche sur yon"
        )
        Restaurant.objects.create(
            id=1,
            name="Test",
            address=adresse
        )
        ingredient = Ingredient.objects.create(
            id=1,
            name="fromage"
        )
        pizzaF = Pizza.objects.create(
            id = 1,
            name="pizza au fromage",
            price=10,
        )
        pizzaF.ingredients.add(ingredient)

    def test_restaurant_existe(self):
        restaurant = Restaurant.objects.get(pk=1)
        self.assertEqual(
            "Test at 16, rue des peuplier () - la roche sur yon (85000)",
            str(restaurant)
        )

    def test_ingredient_existe(self):
        ing = Ingredient.objects.get(pk=1)
        self.assertEqual(
            "Ingredient: fromage (allergen: no)",
            str(ing)
        )
    def test_pizzas_existe(self):
        pizz = Pizza.objects.get(pk=1)
        self.assertEqual(
            "Pizza pizza au fromage (10€)",
            str(pizz)
        )